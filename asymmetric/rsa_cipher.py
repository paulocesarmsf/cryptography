from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP


class RSACipher(object):

    # key_lenght 1024, 2048, 3072 (2048 is recommended)
    def key_generate(self, key_lenght=2048):
        keyPair = RSA.generate(key_lenght)
        pubKey = keyPair.publickey()
        return pubKey.exportKey(), keyPair.exportKey()

    def encryption(self, msg, pubkey_pem):
        pukey = RSA.importKey(pubkey_pem.decode('ascii'))
        encryptor = PKCS1_OAEP.new(pukey)
        return encryptor.encrypt(msg.encode())

    def decryption(self, msg, privkey_pem):
        prkey = RSA.importKey(privkey_pem.decode('ascii'))
        decryptor = PKCS1_OAEP.new(prkey)
        return decryptor.decrypt(msg)


rsa_asymetric_cripto = RSACipher()
pubKeyPEM, privKeyPEM = rsa_asymetric_cripto.key_generate()
encrypted = rsa_asymetric_cripto.encryption('A message for encryption', pubKeyPEM)
decrypted = rsa_asymetric_cripto.decryption(encrypted, privKeyPEM)

print(f"Encrypted: {encrypted}")
print(f"Decrypted: {decrypted}")
